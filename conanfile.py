from conans import ConanFile, CMake, tools
from conans.errors import ConanInvalidConfiguration


class VcmlConan(ConanFile):
    name = "vcml"
    version = "37f53cc"
    description = """A modeling library with virtual components for
SystemC and TLM simulators"""
    homepage = "https://github.com/janweinstock/vcml"
    url = "https://gitlab.com/dxplore/conan/conan-vcml"
    license = "Apache-2.0"
    author = "Jan Van Winkel <jan.van_winkel@dxplore.eu>"
    topics = ("simulation", "modeling", "esl", "tlm")
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "build_utils": [True, False],
    }
    default_options = {
        "shared": True,
        "build_utils": True
    }
    generators = "cmake"
    requires = "SystemC/2.3.3@dxplore/stable", "libelf/0.8.13"
    exports_sources = "vcml.patch"

    def configure(self):

        if tools.valid_min_cppstd(self, "17"):
            raise ConanInvalidConfiguration(
                "C++ Standard %s not supported by SystemC" %
                self.settings.compiler.cppstd)

        if not self.settings.compiler.cppstd:
            if tools.valid_min_cppstd(self, "14"):
                self.settings.compiler.cppstd = 14
            elif tools.valid_min_cppstd(self, "11"):
                self.settings.compiler.cppstd = 11
            elif tools.valid_min_cppstd(self, "98"):
                self.settings.compiler.cppstd = 98

    def source(self):
        git = tools.Git(folder="src")
        git.clone(url="https://github.com/janweinstock/vcml.git")
        git.checkout(self.version)
        tools.patch(base_path="src", patch_file="vcml.patch")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["VCML_BUILD_UTILS"] = self.options.build_utils
        cmake.configure(source_folder="src")
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.h", dst="include", src="package/include")
        self.copy("*.so", dst="lib", src="package/lib")
        self.copy("*.a", dst="lib", src="package/lib")
        self.copy("*", dst="bin", src="package/bin")

    def package_info(self):
        self.cpp_info.libs = ["vcml"]

        if (self.settings.os == "Linux") and not self.options.shared:
            self.cpp_info.libs.append("pthread")
